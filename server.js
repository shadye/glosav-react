var url = require('url');
var path = require('path');
var express = require('express');
var request = require('request');
var webpack = require('webpack');
var config = require('./webpack.config');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');

var app = express();
var compiler = webpack(config);

app.use(bodyParser());
app.use(cookieParser());

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

app.get('/', function (req, res) {
  console.log(req.url);
  res.sendFile(path.join(__dirname, './static/index.html'));
});

app.use('/webapi/rest/public/auth/getToken', function(req,res){
  var link = "http://10.0.5.144:8080/webapi/rest/public/auth/getToken"

  request({
    url: link,
    headers: {
      'Content-Type'    : 'application/json; charset=UTF-8',
      'Accept'          : '*/*',
      'Accept-Encoding' : 'gzip, deflate',
      'Accept-Language' : 'en-US,en;q=0.8'
    },
    method: 'POST',
    body: JSON.stringify(req.body)
  }, function(error, response, body){
    if ((!error) && (response.statusCode === 200)) {
      res.send(body);
    } else {
      console.log("request error", body);
    }
  });
});


app.use('/webapi', function(req,res){
  var auth = req.headers.authorization;
  var parts = url.parse(req.url);
  var link = "http://10.0.5.144:8080/webapi"+parts.href;
  console.log(link);

  request({
    url: link,
    headers: {
      Authorization: auth
    }
  }, function(error, response, body){
    if (!error && response.statusCode === 200) {
      res.send(body);
    } else {
      console.log("request error", error);
    }
  });
});

app.listen(3000, function (err) {
  if (err) {
    console.error(err);
    return;
  }

  console.log('Listening at http://localhost:3000');
});
