
import React, { Component } from 'react'
import classnames from 'classnames'
import style from './style.css'
import { Paper } from 'material-ui'
import _ from 'underscore'


class Map extends Component {
  constructor(props, context) {
    super(props, context)
    var timer = setInterval( ()=> {
      if (window.ymaps) {
        ymaps.ready(::this.init)
        clearTimeout(timer)
        console.log(timer);
      }
    }, 200)
  }

  init() {
    const Map = new ymaps.Map ("map", {
      center: [55.76, 37.64],
      zoom: 7
    })
    this.props.createYmap(Map)
  }

  render() {

    const { instance, objects, track } = this.props

    if (track) {
      instance.geoObjects.removeAll()
      const defaultTrack = new ymaps.Polyline(track.points,
      {
        hintContent: track.name
      }, {
        strokeColor: '#ff4081',
        strokeWidth: 5,
        strokeOpacity: .8
      })
      instance.geoObjects.add(defaultTrack)
      instance.setBounds(defaultTrack.geometry.getBounds())
    }

    if (objects) {
      instance.geoObjects.removeAll()
      const ObjectManager = new ymaps.ObjectManager({ clusterize: true })
      let collection = {
        type: 'FeatureCollection',
        features: []
      }
      let id = 0
      _.mapObject(objects, (data, id)=> {
        const { latitude, longitude, name } = data
        collection.features.push({
          type: 'Feature',
          id: id++,
          geometry: {
            type: 'Point',
            coordinates: [latitude, longitude]
          },
          properties: {
            balloonContent: name
          }
        })
      })
      ObjectManager.add(collection)
      instance.geoObjects.add(ObjectManager)
      instance.setBounds(ObjectManager.getBounds())
    }

    return (
      <Paper className={style.mapContainer}>
        <div className={style.map} id="map"></div>
      </Paper>
    )
  }
}

export default Map
