
import React, { Component } from 'react'
import classnames from 'classnames'
import style from './style.css'
import {
  FlatButton,
  LeftNav,
  Paper,
  List,
  ListItem,
  IconButton,
  IconMenu,
  MenuItem,
  Dialog,
  CircularProgress,
  DatePicker,
  TimePicker,
  TextField,
  Badge,
  SelectField,
  Snackbar } from 'material-ui'
import {
  CommunicationBusiness,
  FileFolder,
  MapsDirectionsCar,
  HardwareKeyboardArrowRight,
  NavigationMoreVert,
  MapsMyLocation } from 'material-ui/lib/svg-icons'
import {SelectableContainerEnhance} from 'material-ui/lib/hoc/selectable-enhance'
import api from '../../service/api'
import _ from 'underscore'
import moment from 'moment'

const SelectableList = SelectableContainerEnhance(List)

class HierarchicalList extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      modalOpen: false,
      modalActions: <i />,
      dateFrom: null,
      dateTo: null,
      currentDevice: {},
      showDates: false,
      dateSelect: '',
      snack: false,
      snackMessage: '',
      enter: true
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.list.current && this.props.list.current) {
      if (nextProps.list.current.length != this.props.list.current.length) {
        this.setState({ enter: true })
        setTimeout(()=>{
          this.setState({ enter: false })
        }, 650)
      }
    }
  }

  handleCloseModal() {
    this.setState( { modalOpen: false } )
  }

  handleDateChange(e) {
    this.setState( { [e.target.name]: moment(e.target.value).unix() } )
  }

  handleDateSelect(e, index, value) {
    console.log(e)
    let dateTo = null
    let dateFrom = null
    switch (e.target.innerHTML) {
      case 'Вчера':
        this.setState( {showDates: false} )
        dateFrom = moment().set( {hour:0, minute:0, seconds:0} ).add(-2, 'days').unix()
        dateTo = moment().set( {hour:0, minute:0, seconds:0} ).add(-1, 'days').unix()
        break
      case 'Ввести вручную':
        this.setState( {showDates: true} )
        break
      default:
        this.setState( {showDates: false} )
        dateTo = moment().unix()
        dateFrom = dateTo - value
        break
    }
    this.setState( { dateTo: dateTo, dateFrom: dateFrom, dateSelect: value } )
  }

  openModal(id, kind) {
    const actions = [
      <FlatButton
        label="Отмена"
        secondary={true}
        onTouchTap={::this.handleCloseModal}
      />,
      <FlatButton
        label="Отправить"
        primary={true}
        keyboardFocused={true}
        onTouchTap={::this.getTrack}
      />
    ]
    this.setState( { modalActions: actions, modalOpen: true, currentDevice: { id: id, kind: kind} } )
  }

  getTrack() {
    const me = this
    const { application, createTrack, list, loadChart } = this.props
    const { currentDevice, dateFrom, dateTo } = this.state
    let reportLink = `/webapi/rest/private/channels/report?d=${currentDevice.id}&begin=${dateFrom}&end=${dateTo}`
    _.mapObject(application.channels, id => reportLink += `&ch=${id}`)

    api(`/webapi/rest/private/telematics/period?begin=${dateFrom}&end=${dateTo}&${currentDevice.kind}=${currentDevice.id}`, application.token)
      .then(track=> {
        _.mapObject(track, (val, key)=> {
          let name = list.hash[`d${key}`].name
          let pointsArray = []
          if (val.length) {
            val.map( point => pointsArray.push( [point.latitude, point.longitude] ) )
            createTrack({ name: name, points: pointsArray})
          } else {
            me.setState( { snack: true, snackMessage: 'За данный период нечего отображать' } )
          }
        })
      })
    api(reportLink, application.token)
      .then(chart => loadChart(chart))

    this.handleCloseModal()
  }

  getPosition(id, kind) {
    const me = this
    const { application, list, replaceObjects } = this.props
    async function position(id, kind) {
      try {
        let position = await api(`/webapi/rest/private/telematics/position?${kind}=${id}`, application.token)
        console.log(position)
        if (!_.isEmpty(position)) {
          _.mapObject(position, (val, key)=>{
            position[key].name = list.hash[`d${key}`].name
          })
          replaceObjects(position)
        } else {
          me.setState( { snack: true, snackMessage: 'Нечего отображать' } )
        }
      } catch(e) {
        console.log(e);
      }
    }
    position(id, kind)
  }

  onListTouch(key, kind) {
    const { forward } = this.props
    if (kind != 'd') forward(key)
  }

  closeSnack() {
    this.setState( { snack: false, snackMessage: '' } )
  }

  render() {
    const me = this
    const { modalOpen, modalActions } = this.state
    const { list } = this.props

    let root = <CircularProgress mode="indeterminate" />

    const tempRoot = []

    let keyCount = 0

    function renderItems(item) {
      let shortKind = item.kind.slice(0, 1).toLowerCase()
      let attr = {
        primaryText: item.name,
        disableKeyboardFocus: true,
        key: keyCount++
      }

      const showLocationButton = (
        <IconButton
          touch={true}
          tooltip="Местоположение"
          tooltipPosition="bottom-left"
          onTouchTap={me.getPosition.bind(me, item.id, shortKind)}>
          <MapsMyLocation />
        </IconButton>
      )

      const iconButtonElement = (
        <IconButton
          touch={true}
          tooltip="Действия"
          tooltipPosition="bottom-left">
          <NavigationMoreVert />
        </IconButton>
      )

      const rightIconMenu = (
        <IconMenu iconButtonElement={iconButtonElement}>
          <MenuItem
            onTouchTap={me.getPosition.bind(me, item.id, shortKind)}>Текущее положение</MenuItem>
          <MenuItem
            onTouchTap={me.openModal.bind(me, item.id, shortKind)}>Получить трэк</MenuItem>
        </IconMenu>
      )

      const badgeStyle = {
        padding: "5px 17px 4px 12px"
      }

      switch (item.kind) {
        case 'COMPANY':
          attr.leftIcon = <Badge style={badgeStyle} badgeContent={item.childs.length} primary={true} ><CommunicationBusiness /></Badge>
          attr.rightIconButton = showLocationButton
          break
        case 'GROUP':
          attr.leftIcon = <Badge style={badgeStyle} badgeContent={item.childs.length} primary={true} ><FileFolder /></Badge>
          attr.rightIconButton = showLocationButton
          break
        case 'DEVICE':
          attr.leftIcon = <MapsDirectionsCar />
          attr.rightIconButton = rightIconMenu
          break
        default:
          break
      }

      attr.onTouchTap = me.onListTouch.bind(me, attr.key, shortKind)

      return (<ListItem { ...attr }/>)
    }

    if (list.current) {

      list.current.map( child => tempRoot.push(renderItems(child)) )
      root = tempRoot

    }

    return (
        <LeftNav open={true} docked={true} className={style.paper}>
        <List className={classnames( { [style.list]: true, [style.listEnter]: me.state.enter } ) }>
          {
            root
          }
        </List>
        <Dialog
          title="Выбор даты и времени"
          open={modalOpen}
          actions={modalActions}
          onRequestClose={::me.handleCloseModal} >
          <SelectField value={me.state.dateSelect} onChange={::me.handleDateSelect}>
            <MenuItem value={1800} primaryText="30 минут"/>
            <MenuItem value={21600} primaryText="6 часов"/>
            <MenuItem value={86400} primaryText="Сегодня"/>
            <MenuItem value={0} primaryText="Вчера"/>
            <MenuItem value={172800} primaryText="2 дня"/>
            <MenuItem value={604800} primaryText="1 неделя"/>
            <MenuItem value={2629743} primaryText="1 месяц"/>
            <MenuItem value={1} primaryText="Ввести вручную"/>
          </SelectField>
          <div className={classnames( {[style.dates]: true, [style.activeDates]: me.state.showDates} ) }>
            <TextField name="dateFrom" type="datetime-local" onChange={::me.handleDateChange} />
            <TextField name="dateTo" type="datetime-local" onChange={::me.handleDateChange} />
          </div>
        </Dialog>
        <Snackbar
          open={me.state.snack}
          message={me.state.snackMessage}
          autoHideDuration={3000}
          onRequestClose={::me.closeSnack}/>
      </LeftNav>
    )
  }
}

export default HierarchicalList
