
import React, { Component } from 'react'


class List extends Component {


  render() {

    const { childs, header } = this.props

    return (
      <ul>
        <span>{header}</span>
        { childs.map( child => <li>{ child }</li>) }
      </ul>
    )
  }
}

export default List
