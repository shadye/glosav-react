import React, { Component } from 'react'
import classnames from 'classnames'
import style from './style.css'
import { Paper } from 'material-ui'
import d3 from 'd3'

export default class Chart extends Component {

  constructor(props) {
    super(props)
  }

  renderd3(json) {
    const margin = {
      top: 20,
      right: 20,
      bottom: 30,
      left: 50
    }

    const width = 960 - margin.left - margin.right
    const height = 500 - margin.top - margin.bottom

    const formatDate = d3.time.format("%d-%b-%y")

    const x = d3.time.scale()
      .range([0, width])

    const y = d3.scale.linear()
      .range([height, 0])

    const xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom")

    const yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")

    // const line = d3.svg.line()
    //   .x( d => x(d.date) )
    //   .y( d => y(d.close) )

    console.log(json)
    // d3.json(json, data => {
    //   // x.domain(d3.extend(data, d => return d.))
    // })
  }

  render() {

    // const { json } = this.props
    //
    // if ( json ) console.log(json)

    return(
      <Paper className={style.container}>

      </Paper>
    )
  }

}
