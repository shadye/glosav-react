import reqwest from 'reqwest'

export default function api(url, token) {
  return new Promise((resolve, reject) => {
    reqwest({
      url: url,
      headers: {
        'Authorization': `TokenAuth auth=${token}`
      }
    })
    .then(response => {
      resolve(JSON.parse(response))
    })
  })
}
