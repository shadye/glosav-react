
import { createStore, applyMiddleware, compose } from 'redux'
import { syncHistory } from 'redux-simple-router'
import { browserHistory } from 'react-router'
import persistState from 'redux-localstorage'

import { logger } from '../middleware'
import rootReducer from '../reducers'

export function configure(initialState) {
  const createPersistentStore = compose(
    persistState()
  )(createStore)

  const createStoreWithMiddleware = applyMiddleware(
    logger,
    syncHistory(browserHistory)
  )(createPersistentStore)

  const store = createStoreWithMiddleware(rootReducer, initialState)

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers')
      store.replaceReducer(nextReducer)
    })
  }
  return store
}
