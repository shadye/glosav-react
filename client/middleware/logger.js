
export default store => next => action  => {
  // console.log(action)
  // return next(action)
  console.group()
    console.log('will dispatch', action)
    const result = next(action)
    console.log('state after dispatch', store.getState())
    console.groupEnd()

    return result
}
