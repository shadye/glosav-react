
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import injectTapEventPlugin from "react-tap-event-plugin";
import { routeActions } from 'redux-simple-router'
import _ from 'underscore'

import { NavigationMoreVert, NavigationArrowBack } from 'material-ui/lib/svg-icons'
import { AppBar, IconButton, IconMenu, MenuItem } from 'material-ui'
import HierarchicalList from '../../components/HierarchicalList'
import Ymap from '../../components/Ymap'
import Chart from '../../components/Chart'

import * as ListActions from '../../actions/list'
import * as ApplicationActions from '../../actions/application'
import * as ymapActions from '../../actions/ymap'
import * as ChartActions from '../../actions/chart'

import api from '../../service/api'

import style from './style.css'

injectTapEventPlugin()

class Main extends Component {

  constructor(props) {
    super(props)
    const { listActions, applicationActions, application } = props
    api('/webapi/rest/private/hs/root?recursive=true', application.token)
      .then(list => listActions.load(list))
    _.mapObject(application.channels, (id, name) => {
      if (id === null) {
        api(`/webapi/rest/private/channels/channel/${name}`, application.token)
          .then(channel => applicationActions.getChannelId({name: name, id: channel.id}))
      }
    })
  }

  logOut() {
    const { listActions, applicationActions, history, ymap } = this.props
    listActions.clear()
    applicationActions.logOut()
    ymap.instance.geoObjects.removeAll()
    history.push({}, '/login')
    routeActions.push('/login')
  }

  render() {
    const {
      application,
      list,
      ymap,
      chart,
      ymapActions,
      listActions,
      chartActions,
      children } = this.props

    let backButton = <i/>
    if (list.history.length > 0) {
      backButton = <IconButton onTouchTap={listActions.backward}><NavigationArrowBack /></IconButton>
    }

    return (
      <div className={style.normal}>
        <AppBar
          iconElementLeft={backButton}
          onLeftIconButtonTouchTap={listActions.backward}
          iconElementRight={
            <IconMenu
              iconButtonElement={
                <IconButton><NavigationMoreVert /></IconButton>
              }
              targetOrigin={{horizontal: 'right', vertical: 'top'}}
              anchorOrigin={{horizontal: 'right', vertical: 'top'}}
            >
              <MenuItem primaryText="Выход" onTouchTap={::this.logOut}/>
            </IconMenu>
          }
        />
      <HierarchicalList
        list={list}
        application={application}
        replaceObjects={ymapActions.replaceObjects}
        createTrack={ymapActions.createTrack}
        forward={listActions.forward}
        backward={listActions.backward}
        loadChart={chartActions.load} />
      <Ymap
        instance={ymap.instance}
        objects={ymap.objects}
        track={ymap.track}
        createYmap={ymapActions.createYmap} />
      <Chart json={chart.json}/>
        {children}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    list: state.list,
    application: state.application,
    ymap: state.ymap,
    chart: state.chart
  }
}

function mapDispatchToProps(dispatch) {
  return {
    listActions: bindActionCreators(ListActions, dispatch),
    applicationActions: bindActionCreators(ApplicationActions, dispatch),
    ymapActions: bindActionCreators(ymapActions, dispatch),
    chartActions: bindActionCreators(ChartActions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main)
