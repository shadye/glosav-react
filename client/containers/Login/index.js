import React, { PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Paper, TextField, RaisedButton } from 'material-ui'
import * as ApplicationActions from '../../actions/application'
import reqwest from 'reqwest'
import { routeActions } from 'redux-simple-router'
import style from './style.css'

class Login extends React.Component {

  constructor (props) {
    super(props)
    this.state = { username: null, password: null }
    console.log(this)
  }

  handleInputChange (evt) {
    this.setState({
      [evt.target.name]: evt.target.value
    })
  }

  handleSubmit (evt) {
    evt.preventDefault()
    const { actions, history } = this.props
    reqwest({
      url: '/webapi/rest/public/auth/getToken',
      method: 'POST',
      data: this.state
    }).then((response)=>{
      actions.logIn(response)
      history.push({}, '/main')
      routeActions.push('/main')
    })
  }

  render () {
    return (
      <Paper className={style.login}>
        <form
          onChange={::this.handleInputChange}
          onSubmit={::this.handleSubmit}>
          <TextField
            className={style.input}
            floatingLabelText="Логин"
            hintText="user@company"
            name="username"/>
          <TextField
            className={style.input}
            floatingLabelText="Пароль"
            type="password"
            name="password"/>
          <div className={style.submit}>
            <RaisedButton
              className={style.submitButton}
              secondary={Boolean(true)}
              label="войти"
              type="submit"/>
          </div>
        </form>
      </Paper>
    )
  }
}

function mapStateToProps(state) {
  return {
    application: state.application
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ApplicationActions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)
