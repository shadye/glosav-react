
import { Router, Route, browserHistory } from 'react-router'
import { Provider } from 'react-redux'
import ReactDOM from 'react-dom'
import React from 'react'
import { routeActions } from 'redux-simple-router'
import 'react-virtualized/styles.css';

import Login from './containers/Login'
import Main from './containers/Main'
import { configure } from './store'

// window.localStorage.removeItem('redux')
const store = configure()

const transition = (nextState, replace, path) => {
  replace({
      pathname: path,
      state: { nextPathname: nextState.location.pathname }
    })
  routeActions.push(path)
}

const checkAuth = (nextState, replace) => {
  const state = store.getState()
  const isLoggedIn = Boolean(state.application.token)
  if ( isLoggedIn ) {
    transition(nextState, replace, '/main')
  } else {
    transition(nextState, replace, '/login')
  }
}

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" onEnter={checkAuth} />
      <Route path="/Main" component={Main} />
      <Route path="/login" component={Login} />
    </Router>
  </Provider>,
  document.getElementById('root')
)
