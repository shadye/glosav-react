
import { handleActions } from 'redux-actions'

let hash = {}

function createHashObject(payload) {
  payload.childs.map( item => {
    hash[`${item.kind.slice(0, 1).toLowerCase()}${item.id}`] = item
    if (item.childs) {
      createHashObject(item)
    }
  } )
}

const initialState = {
  // raw: null,
  hash: null,
  current: null,
  history: []
}

export default handleActions({
  'load': (state, action) => {
    createHashObject(action.payload)
    return Object.assign({}, state, {
      // raw: action.payload,
      hash: hash,
      current: action.payload.childs
    })
  },

  'forward': (state, action) => {
    return Object.assign({}, state, {
      history: [...state.history, state.current],
      current: state.current[action.payload].childs
    })
  },

  'backward': (state, action) => {
    return Object.assign({}, state, {
      current: state.history.pop(),
      history: state.history
    })
  },

  'clear': (state, action) => {
    return initialState
  }
}, initialState)
