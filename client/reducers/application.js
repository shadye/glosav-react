
import { handleActions } from 'redux-actions'

const initialState = {
  token: null,
  channels: {
    speed: null,
    path: null,
    fuel: null,
    parking: null,
    engine: null
  }
}

export default handleActions({

  'log in': (state, action) => {
    return Object.assign({}, state, {
      token: action.payload
    })
  },

  'log out': (state, action) => {
    return Object.assign({}, state, {
      token: null
    })
  },

  'get channel id': (state, action) => {
    const { payload } = action
    return Object.assign({}, state, {
      channels: Object.assign({}, state.channels, {
        [payload.name]: payload.id
      })
    })
  }
}, initialState)
