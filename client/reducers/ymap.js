
import { handleActions } from 'redux-actions'

const initialState = {
  instance: null,
  objects: null,
  track: null
}

export default handleActions({
  'create ymap': (state, action) => {
    return Object.assign({}, state, {
      instance: action.payload
    })
  },

  'replace objects': (state, action) => {
    return Object.assign({}, state, {
      objects: action.payload
    })
  },

  'create track': (state, action) => {
    return Object.assign({}, state, {
      track: action.payload
    })
  }
}, initialState)
