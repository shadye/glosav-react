
import { routeReducer as routing } from 'redux-simple-router'
import { combineReducers } from 'redux'
import application from './application'
import list from './list'
import ymap from './ymap'
import chart from './chart'

export default combineReducers({
  routing,
  application,
  list,
  ymap,
  chart
})
