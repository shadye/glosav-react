
import { handleActions } from 'redux-actions'

const initialState = {
  json: null
}

export default handleActions({
  'load': (state, action) => {
    return Object.assign({}, state, {
      json: action.payload
    })
  }
}, initialState)
