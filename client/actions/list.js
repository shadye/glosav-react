
import { createAction } from 'redux-actions'

export const load = createAction('load')
export const clear = createAction('clear')
export const forward = createAction('forward')
export const backward = createAction('backward')
