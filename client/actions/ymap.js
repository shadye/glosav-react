
import { createAction } from 'redux-actions'

export const createYmap = createAction('create ymap')
export const replaceObjects = createAction('replace objects')
export const createTrack = createAction('create track')
