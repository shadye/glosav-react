
import { createAction } from 'redux-actions'

export const logIn = createAction('log in')
export const logOut = createAction('log out')
export const getChannelId = createAction('get channel id')
